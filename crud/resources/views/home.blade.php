@extends('layouts.default')

@section('content')
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h3>Dashboard</h3>
        </div>
        <div class="pull-right dropdown" >            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="menu" aria-expanded="false" style="text-decoration: none;" > {{ Auth::user()->name }} <span class="caret"></span> </a> 
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>            
        </div>
    </div>    
</div>

<br>
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p> {{$message}} </p>
    </div>
@endif

<table class="table table-bordered table-striped">
    <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Email</th>
        <th width="210px">Action</th>
    </tr>
   
    @foreach($data as $d) 

    <tr>        
        <td> {{ $d->id }} </td>
        <td> {{ $d->name }} </td>
        <td> {{ $d->email }} </td>
        <td>
            <a class="btn btn-primary" href=" {{ route('edit',$d->id) }} ">Edit</a>            
            {!! Form::open(['method'=>'DELETE','route'=>['destroy'],'style'=>'display:inline']) !!}
            {!! Form::hidden('id', $d->id) !!}
            {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>        
    </tr>   
    @endforeach  
 
</table>

@endsection