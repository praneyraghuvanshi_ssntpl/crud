@extends('layouts.default')

@section('content')

<br>
<div class="row">
	<div class="col-lg-12">
		<div class="pull-left">
			<h3>Login</h3>
		</div>
		<div class="pull-right">
			<a class="btn btn-primary" href=" {{ route('welcome') }} ">Back</a>
		</div>
	</div>
</div>
@if(count($errors)>0)
<br>
	<div class="alert alert-danger">
		<strong>Oops!!</strong>There were some problems with your input.<br><br>
		<ul>
			@foreach($errors->all() as $error)
			<li> {{ $error }} </li>
			@endforeach
		</ul>
	</div>
@endif
@if($message = Session::get('failure'))
<br>
    <div class="alert alert-danger">
        <p> {{$message}} </p>
    </div>
@endif
<br>
{!! Form::open(array('route'=>'home','method'=>'POST')) !!}
	<div class="row">	
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="form-group">
			<strong>Email:</strong>
			{!! Form::text('email',null,array('placeholder'=>'Email','class'=>'form-control')) !!}
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="form-group">
			<strong>Password:</strong>
			{!! Form::password('password',['class'=>'form-control']) !!}
		</div>
	</div>	
	<div class="col-xs-12 col-md-12 col-sm-12">
		<button type="submit" class="btn btn-primary">Login</button>
	</div>
</div>
{!! Form::close() !!}

@endsection