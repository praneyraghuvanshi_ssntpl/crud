@extends('layouts.default')

@section('content')

<br>
<div class="row">
	<div class="col-lg-12">
		<div class="pull-left">
			<h3>Register</h3>
		</div>
		<div class="pull-right">
			<a class="btn btn-primary" href=" {{ route('welcome') }} ">Back</a>
		</div>
	</div>
</div>
@if(count($errors)>0)
<br>
	<div class="alert alert-danger">
		<strong>Oops!!</strong>There were some problems with your input.<br><br>
		<ul>
			@foreach($errors->all() as $error)
			<li> {{ $error }} </li>
			@endforeach
		</ul>
	</div>
@endif
<br>
{!! Form::open(array('route'=>'store','method'=>'POST')) !!}
	@include('form')
{!! Form::close() !!}

@endsection