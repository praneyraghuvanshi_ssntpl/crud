@extends('layouts.default')

@section('content')

<br>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h3>CRUD Application</h3>
        </div> 
        <div class="pull-right">
            <a class="btn btn-success" href=" {{ route('insert') }} ">Add New User</a>
            <a class="btn btn-primary" href=" {{ route('login') }} ">Login</a>
        </div>
    </div>
</div>
<br>

@if($message = Session::get('success') )
    <div class="alert alert-success">
        <p> {{$message}} </p>
    </div>    
@endif
@if($message = Session::get('failure'))
    <div class="alert alert-danger">
        <p> {{$message}} </p>
    </div>
@endif

<table class="table table-bordered table-striped">
    <tr>
        <th> ID </th>
        <th> Name </th>
        <th> Email </th>
    </tr>
    @foreach($users as $user)
    <tr>
        <td> {{$user->id}} </td>
        <td> {{$user->name}} </td>
        <td> {{$user->email}} </td>
    </tr>
    @endforeach
</table>

@endsection