<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="form-group">
			<strong>Name:</strong>
			{!! Form::text('name',null,array('placeholder'=>'Name','class'=>'form-control')) !!}
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="form-group">
			<strong>Email:</strong>
			{!! Form::text('email',null,array('placeholder'=>'Email','class'=>'form-control')) !!}
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="form-group">
			<strong>Password:</strong>
			{!! Form::password('password',['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="col-xs-12 col-md-12 col-sm-12">
		<div class="form-group">
			<strong>Confirm Password:</strong>
			{!! Form::password('password_confirmation',['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="col-xs-12 col-md-12 col-sm-12">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</div>