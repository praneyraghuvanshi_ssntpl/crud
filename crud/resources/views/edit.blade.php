@extends('layouts.default')

@section('content')

<br>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h3>Dashboard : Edit Details</h3>
        </div>
        <div class="pull-right dropdown" >            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="menu" aria-expanded="false" style="text-decoration: none;" > {{ Auth::user()->name }} <span class="caret"></span> </a> 
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>            
        </div>
    </div>    
</div>

<br>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    {!! Form::model($user, ['method'=>'PUT','route' => ['update']]) !!}
    {!! Form::hidden('id', $user->id) !!}
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="form-group">
			<strong>Name:</strong>
			{!! Form::text('name',null,array('placeholder'=>'Name','class'=>'form-control')) !!}
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="form-group">
			<strong>Email:</strong>
			{!! Form::text('email',null,array('placeholder'=>'Email','class'=>'form-control')) !!}
		</div>
	</div>	
	<div class="col-xs-12 col-md-12 col-sm-12">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</div>
    {!! Form::close() !!}

@endsection