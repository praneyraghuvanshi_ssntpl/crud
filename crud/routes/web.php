<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/','CrudController@index')->name('welcome');
Route::get('/create','CrudController@create')->name('insert');
Route::post('/store','CrudController@store')->name('store');


Route::get('/login','LoginController@login')->name('login');
Route::post('/home','LoginController@authenticate')->name('home');
Route::get('/home','LoginController@show')->name('show');


Route::get('/home/edit/{id}','HomeController@edit')->name('edit');
Route::put('/home','HomeController@update')->name('update');
Route::delete('/home/delete','HomeController@destroy')->name('destroy');
Route::post('/logout','HomeController@logout')->name('logout');
