<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Session;

class LoginController extends Controller
{    
    public function authenticate(Request $request)
    {
       
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            $data = User::where('email', $request->email)->get();

            $request->session()->put('s_name',$request->email);            
            return view('home',compact('data'));          
        }
        else
        {
        	return redirect()->route('login')
    					->with('failure','Wrong user credentials');
        }
    }

    public function login()
    {
    	return view('login');
    }

    public function show()
    { 
    	if (session()->has('s_name')) {
    		return view('home');
		}     
    	return redirect()->route('login')->with('failure','You are not logged-in');
    }
    
    
}
