<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Hash;

class CrudController extends Controller
{
    public function index()
    {
    	$users = User::all();
    	return view('welcome',compact('users'));
    }

    public function create()
    {
    	return view('register');
    }

    public function store(Request $request)
    {
    	$this->validate($request,['name'=>'required|string',
    		'email'=>'required|email|string|unique:users',
    		'password'=>'required|min:6|confirmed']);
    	
    	$user = new User;
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = Hash::make($request->password);
    	$user->save();

    	return redirect()->route('welcome')
    					->with('success','User data successfully inserted to database.');
    }
}
