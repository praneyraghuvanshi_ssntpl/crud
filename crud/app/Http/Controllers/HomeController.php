<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Session;

class HomeController extends Controller
{
	public function edit($id)
	{
        if (session()->has('s_name')) {

        $user = User::find($id);
        return view('edit',compact('user'));           
        }     
        return redirect()->route('login')->with('failure','You are not logged-in');
		
	}

	public function update(Request $request)
	{
        if(session()->has('s_name')) {
        $id = $request->id;

        $this->validate($request, ['name'=>'required','email'=>'required|email']);

        User::find($id)->update($request->all());

        //$request->session()->put('s_name',$request->email);

        $data = User::where('id', $request->id)->get();//User::find($id);

        return view('home',compact('data'));
        }
        //return redirect()->route('show')->with('success','Your details updated successfully');
	}
	
    public function logout(Request $request)
    {
    	$request->session()->forget('s_name');
    	return redirect()->route('welcome')
    	->with('success','You are successfully logged out.');
    }	
	
    public function destroy(Request $request)
    {
    	// $id = Auth::user()->id;
    	$id = $request->id;
    	User::find($id)->delete();
    	$request->session()->forget('s_name');
    	return redirect()->route('welcome')->with('success','User details deleted successfully');
    }
}
